# Git Your Pi

A Python Web App to create public Git repositories on a Raspberry Pi. This project uses the Flask micro-framework to maintain low latency access and use. 

## Git Repos

Git Your Pi lets you easily create public repos. At the moment, the only repos a user can create are public one. But work is being made so that private ones can be created. 

## Soon to come

As the project moves on, private Git repos will become available. This will require Users to become a feature in the web app, which I am currently in the process of working on.

Additionally, I would like to create a way for the user to have a blog and have an easy way to create new blog posts.

Lastly, I hope to have the ability to have multiple users on the site. This will allow owners to collaberate on projects with other people.
