#!flask/bin/python
from app import app
DEBUG=True
HOST='0.0.0.0'
PORT=80
app.run(debug=DEBUG, host=HOST, port=PORT)
