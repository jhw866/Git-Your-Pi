import os

# Start up Information
DEBUG = True 
HOST = '0.0.0.0'
PORT = 5000

#CSRF Protection (don't worry, I will fix this)
WTF_CSRF_ENABLED = False

basedir = os.path.abspath(os.path.dirname(__file__))

SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')
