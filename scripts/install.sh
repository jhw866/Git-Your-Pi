#!/bin/bash

###################################################
# Use this script to install flask on your machine#
###################################################

# Various installation files
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install python-virtualenv

# Get rid of old flask
rm -rf flask/

# Set up environment
virtualenv flask

# Flask Environement
flask/bin/pip install flask
flask/bin/pip install flask-wtf
flask/bin/pip install flask-sqlalchemy
flask/bin/pip install sqlalchemy-migrate
