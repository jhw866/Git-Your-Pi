#!flask/bin/python
from app import app
from config import DEBUG, HOST, PORT
app.run(debug=DEBUG, host=HOST, port=PORT)
