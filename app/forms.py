from flask.ext.wtf import Form
from wtforms import StringField, BooleanField, PasswordField, RadioField
from wtforms.validators import DataRequired

class LoginForm(Form):
	username = StringField('Username', validators=[DataRequired()])
	password = PasswordField('Password', validators=[DataRequired()])

class CreateProjectForm(Form):
	name = StringField('Project Name', validators=[DataRequired()])
	desc = StringField('Project Description', validators=[DataRequired()])
	public_visibility = RadioField('Visibility', choices=[('true', 'Public'),('false', 'Private')])

