from app import app, db, models
from flask import render_template, redirect
from .forms import LoginForm, CreateProjectForm

title='Jeremy Wenzel'
@app.route('/')
@app.route('/index')
def index():
	return render_template('index.html',
							title=title)

@app.route('/about_me')
def about_me():
	return render_template('about_me.html',
							title=title)

@app.route('/public_projects')
def public_projects():
	projects = models.Project.query.all()
	return render_template('public_projects.html',
							title=title,
							projects=projects)

@app.route('/create_new_project', methods=['GET', 'POST'])
def create_new_project():
	form = CreateProjectForm()
	if form.validate_on_submit():
		visibility = 0
		if (form.public_visibility.data == 'true'):
			visibility = 1

		project =  models.Project(name = form.name.data, description=form.desc.data, isPublic=visibility)

		db.session.add(project)
		db.session.commit()
		return redirect('/public_projects') 
	return render_template('create_new_project.html', title=title, form=form)

@app.route('/login', methods=['GET', 'POST'])
def login():
	form = LoginForm()
	if form.validate_on_submit():
		return redirect('/about_me')
	return render_template('login.html',
							title=title,
							form=form)

@app.errorhandler(404)
def page_not_found(error):
	return render_template('404.html'), 404
