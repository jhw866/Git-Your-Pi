from app import db

class User(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	first_name = db.Column(db.String(24), index=True, unique=False)
	last_name  = db.Column(db.String(24), index=True, unique=False)
	user_name = db.Column(db.String(36), index=True, unique=True)
	email = db.Column(db.String(100), index=True, unique=True)

	def __repr__(self):
		return '<User %r>' % (self.user_name)

class Project(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(140), index=True, unique=True)
	description = db.Column(db.String(300), index=True, unique=False)
	isPublic = db.Column(db.Boolean)

	def __repr__(self):
		return '<Name %r>' % (self.name)
